let pokeTrainer = {
	name: 'Brock',
	age: 15,
	pokemon: ['Steelix', 'Vulpix', 'Ludicolo', 'Blissey', 'Comfey'],
	friends: {
		kanto: ['Ash', 'Misty'],
		hoenn: ['Max','May']
	},
	talk: function(){
		console.log('Vulpix! I choose you!')
	}
}

console.log(pokeTrainer);

console.log("Result of dot notation:");
console.log(pokeTrainer.name);

console.log("Result of square bracket notation:");
console.log(pokeTrainer['pokemon']);

console.log("Result of talk notation:");
pokeTrainer.talk();

function Pokemon(name, level){
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	this.tackle = function(target){
		console.log(this.name + ' tackled ' + target.name);
		reducedHealth = (target.health - this.attack);
		console.log(target.name + "'s health is now reduced to " + (target.health - this.attack));
		if (reducedHealth <= 0){
			target.faint();
		}
	}
	this.faint = function(){
		console.log(this.name + ' fainted');
	}
}

let vulpix = new Pokemon('Vulpix', 50);
let arbok = new Pokemon('Arbok', 33);
let charizard = new Pokemon('Charizard', 57);
let aggron = new Pokemon('Aggron', 46);
let malamar = new Pokemon('Malamar', 19);
let togepi = new Pokemon('Togepi', 23);

console.log(vulpix);
console.log(arbok);
console.log(charizard);
console.log(aggron);
console.log(malamar);
console.log(togepi);

charizard.tackle(arbok);
vulpix.tackle(togepi);
aggron.tackle(malamar);

